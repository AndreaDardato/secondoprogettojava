package com.example.secondoprogetto;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    //variabili utili per capire se siamo in conversione euro-dollaro oppure dollaro-euro
    private boolean tipoConversione = true; //con true si fa conversione euro-dollaro

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //I set my button
        Button reverseButton = findViewById(R.id.btnConverti);
        ImageButton btnIndietro = findViewById(R.id.btnIndietro);
        ImageButton btnInverti = findViewById(R.id.btnInverti);

        reverseButton.setOnClickListener(this);
        btnIndietro.setOnClickListener(this);
        btnInverti.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        EditText euroTxt = findViewById(R.id.editEuro);
        EditText dollariTxt = findViewById(R.id.editDollari);
        TextView euro = findViewById(R.id.euro);
        TextView dollari = findViewById(R.id.dollaro);


        switch (view.getId()) {
            case R.id.btnConverti: {
                double dEuroTxt;
                double risultatoConversioneNumerica;
                String risultatoConversioneStringa;

                if ((euroTxt == null) && (dollariTxt == null)) {
                    Toast message = Toast.makeText(this, "campi vuoti", Toast.LENGTH_SHORT);
                    message.show();
                } else if ((euroTxt == null) && (dollariTxt != null)) {
                    Toast message2 = Toast.makeText(this, "Campo euro vuoto", Toast.LENGTH_SHORT);
                    message2.show();
                } else {
                    //entrambi contengono qualcosa


                    //controllo se euroTxt contiene stringhe

                    try {


                        dEuroTxt = Double.valueOf(euroTxt.getText().toString());

                        //aggiutna controllo se è conversione euro-dolallar o inverso

                        if(this.tipoConversione) {
                            risultatoConversioneNumerica = dEuroTxt * 1.11;
                            risultatoConversioneStringa = String.valueOf(risultatoConversioneNumerica);
                            dollariTxt.setText(risultatoConversioneStringa);
                        }
                        else {
                            risultatoConversioneNumerica = dEuroTxt / 1.11;
                            risultatoConversioneStringa = String.valueOf(risultatoConversioneNumerica);
                            dollariTxt.setText(risultatoConversioneStringa);
                        }



                    }catch(NumberFormatException e) {

                        e.printStackTrace();

                        euroTxt.setText("0");
                        dollariTxt.setText("0");
                        euroTxt = null;
                        dollariTxt = null;
                    }


                }
                break;
            }

            case R.id.btnIndietro: {
                euroTxt.setText("");
                dollariTxt.setText("");
                euroTxt = null;
                dollariTxt = null;
                break;
            }

            case R.id.btnInverti: {

                //deve scambiare i due valori (se ci sono) e scambiare le label
                //se tutti e due i campi sono vuoti si scambia solo le due label

                //prima dello scambio salvo le variabili in altre variabili
                String testoEuro = euro.getText().toString();
                String testoDollari = dollari.getText().toString();
                String euroTxtValue = euroTxt.getText().toString();
                String dollariTxtValue = dollariTxt.getText().toString();

                System.out.println("valore di testoEuro: " + testoEuro);
                System.out.println("valore di testoDollari: " + testoDollari);

                System.out.println("Valore di euroTxtValue: " + euroTxtValue);
                System.out.println("Valore di dollariTxtValue: " + dollariTxtValue);



                //scambio delle label
                euro.setText(testoDollari);
                dollari.setText(testoEuro);

                //fino a qui funziona

                //scambio dei due TextEdit
                euroTxt.setText(dollariTxtValue);
                dollariTxt.setText(euroTxtValue);

                this.tipoConversione = !this.tipoConversione;

                break;
            }

        }

    }



}
